export const CardSections = [
    {
      id: 0,
      title: "Kids Wear",
      subtitle: `Jacket`,
      cards: [
        {
          id: 1,
          title: "lorem ipsum",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641641269/ecomm/fe1_smj64g.jpg",
          subtitle: "This fire extinguisher saved homes from completely burning down. People that used it only had 98% of their homes burned down.",
          text:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface witho"
        },
        {
          id: 2,
          title: "rolem ipsum",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641641269/ecomm/fe2_p8o88c.jpg",
          subtitle: "This is one of the most red fire extinguishers you will ever see. Super clean. It's pretty much useless otherwise.",
          text:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document"
        },
        {
          id: 3,
          title: "Fire Extinguisher, Perfect for Pools",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641641334/ecomm/card1_eulyb9.png",
          subtitle: "If you ever find that your pool is on fire then this is the fire extinguisher for you. It has a 100% success rate.",
          text:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate"
        }
      ]
    },
    {
      id: 4,
      title: "Women's Wear",
      snippet: "Take down that fire lavishly and in style with our luxury extinguishers.",
      cards: [
        {
          id: 5,
          title: "Vintage 1864 Fire Extinguisher",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641644282/ecomm/fe5_ybta2s.jpg",
          subtitle: "This is hands down the worst fire extinguisher you can use in a crisis. The radiation will kill you if the fire doesn't",
          text:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form"
        },
        {
          id: 6,
          title: "Pure Silver & Gold Fire Extinguisher",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641644282/ecomm/index_pp04wc.jpg",
          subtitle: "This one is made out of 50k worth of pure silver and white gold. The metal choice was a poor for functionality, but was great for style!",
          text:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of"
        },
        {
          id: 7,
          title: "Two in One Fire Extinguisher",
          image: "https://res.cloudinary.com/ephraim/image/upload/v1641644281/ecomm/fe4_tyh0y5.jpg",
          subtitle: "This is the first ever two in one fire extinguisher. Good if both you and your friend's houses are burining down",
          content:" publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual"
        }
      ]
    }
  ]